function toggleNav(event) {
  if (document.querySelector(".hamburger-menu").contains(event.target)) {
    document
      .querySelector(".hamburger-menu")
      .classList.toggle("hamburger-menu--active");
    document
      .querySelector(".nav-mobile")
      .classList.toggle("nav-mobile--active");
  } else if (!document.querySelector(".nav-mobile").contains(event.target)) {
    document
      .querySelector(".hamburger-menu")
      .classList.remove("hamburger-menu--active");
    document
      .querySelector(".nav-mobile")
      .classList.remove("nav-mobile--active");
  }
}
document.addEventListener("click", toggleNav);
