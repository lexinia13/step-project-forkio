const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const clean = require("gulp-clean");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const uglify = require("gulp-uglify");
const imagemin = require("gulp-imagemin");
const browserSync = require("browser-sync");
const fileinclude = require("gulp-file-include");
const uncss = require("gulp-uncss-task");
const rename = require("gulp-rename");


gulp.task("uncss", function () {
  gulp
    .src("dist/css/*css")
    .pipe(
      uncss({
        html: ["index.html"],
      })
    )
    .pipe(gulp.dest("index.html"));
});

function minifyImg() {
  return gulp
    .src("src/images/*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/images"));
}

function doClean() {
  return gulp.src("dist/*", { read: false }).pipe(clean());
}

function buildStyles() {
  return gulp
    .src("src/scss/*.scss")
    .pipe(sass.sync().on("error", sass.logError))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(concat("all.css"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("dist/css"));
}

function buildJS() {
  return gulp
    .src("./src/js/*.js")
    .pipe(concat("all.js"))
    .pipe(uglify())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("./dist/js/"));
}

function buildHtml() {
  return gulp
    .src("./src/html/index.html")
    .pipe(
      fileinclude({
        prefix: "@@",
        basepath: "@file",
      })
    )
    .pipe(gulp.dest("./"));
}

function browserSyncServer(cb) {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });
  cb();
}

function browsersyncReload(cb) {
  browserSync.reload();
  cb();
}

function watchStylesJs() {
  gulp.watch(
    ["src/scss/**/*.scss", "src/js/**/*.js"],
    gulp.series(buildStyles, buildJS, browsersyncReload)
  );
}

function watch() {
  gulp.watch(
    ["src/scss/**/*.scss", "src/js/**/*.js", "src/html/**/*.html"],
    gulp.series(
      doClean,
      gulp.parallel(buildStyles, buildJS, buildHtml, minifyImg)
    )
  );
}

function watchNoImg() {
  gulp.watch(
    ["src/scss/**/*.scss", "src/js/**/*.js", "src/html/**/*.html"],
    gulp.parallel(buildStyles, buildJS, buildHtml)
  );
}

exports.buildStyles = buildStyles;
exports.buildJS = buildJS;
exports.doClean = doClean;
exports.buildHtml = buildHtml;
exports.minifyImg = minifyImg;

gulp.task(
  "build",
  gulp.series(doClean, buildStyles, buildJS, buildHtml, minifyImg)
);

exports.dev = gulp.series(
  buildStyles,
  buildJS,
  browserSyncServer,
  watchStylesJs
);

exports.watch = watch;
exports.watchNoImg = watchNoImg;
