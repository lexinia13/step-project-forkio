   ## List of technologies in use

* Programming language used: javascript
* Preprocessor: scss
* Project build: gulp
* Compile scss files into css: gulp-sass
* Add vendor prefixes to CSS properties to support the latest multiple versions of each browser: gulp-autoprefixer
* Remove unused CSS code: gulp-uncss-task
* Concatenate all files into one: gulp-concat
* Cleaning the dist folder: gulp-clean
* Optimize images: gulp-imagemin
* JS code minification: gulp-uglify
* CSS code minification: gulp-clean-css
* HTML file include: gulp-file-include
* Adding the suffix "min": gulp-rename


  ##  Composition of Project Participants

* Front-end developer Viktor Moroz
* Front-end developer Pisotska Oleksandra


  ##  What tasks were performed by each participant

**Front-end developer Viktor Moroz**

* A website header with a top menu
* Section "People Are Talking About Fork"
* Building a project with gulp and gulp plugins

**Front-end developer Pisotska Oleksandra**

* Section "Revolutionary Editor"
* Section "Here is what you get"
* Section "Fork Subscription Pricing"
* Writing README.md